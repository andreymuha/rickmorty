import { Character } from './character.interface';

export interface CharacterAPI {
  info: {
    count: number;
    pages: number;
    next: string | null;
    prev: string | null;
  };
  results: Character[];
}
