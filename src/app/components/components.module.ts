import { incorrectDataPopupComponent } from './incorrectDataPopup/incorrectDataPopup.component';
import { CommonModule } from '@angular/common';
import { MatDialogModule } from '@angular/material/dialog';
import { PopupComponent } from './popup/popup.component';
import { SpinnerComponent } from './spinner/spinner.component';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { HeaderComponent } from './header/header.component';
import { MatNativeDateModule } from '@angular/material/core';

@NgModule({
  imports: [FormsModule, MatDialogModule, MatNativeDateModule],
  exports: [
    HeaderComponent,
    SpinnerComponent,
    PopupComponent,
    incorrectDataPopupComponent,
  ],
  declarations: [
    HeaderComponent,
    SpinnerComponent,
    PopupComponent,
    incorrectDataPopupComponent,
  ],
})
export class ComponentsModule {}
