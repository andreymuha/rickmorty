import { AuthService } from './../../services/auth.service';
import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit {
  @Output()
  public searchTextChanged: EventEmitter<string> = new EventEmitter();
  public text: string;
  constructor(private authService: AuthService, private router: Router) {}
  public ngOnInit(): void {}

  public userToken = JSON.parse(localStorage.getItem('user') as string);

  public logOut(): void {
    this.authService.logout();
    this.router.navigate(['auth']);
  }

  public findUser(): void {
    this.searchTextChanged.next(this.text);
  }
}
