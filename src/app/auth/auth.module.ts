import { MatDialogModule } from '@angular/material/dialog';
import { AuthRoutingModule } from './auth-routing.module';
import { NgModule } from '@angular/core';

import { AuthComponent } from './auth.component';

@NgModule({
  imports: [AuthRoutingModule, MatDialogModule],
  exports: [],
  declarations: [AuthComponent],
  providers: [],
})
export class AuthModule {}
