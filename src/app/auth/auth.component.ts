import { MatDialog } from '@angular/material/dialog';
import { AuthService } from './../services/auth.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { incorrectDataPopupComponent } from '../components/incorrectDataPopup/incorrectDataPopup.component';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.scss'],
})
export class AuthComponent implements OnInit {
  constructor(
    private authService: AuthService,
    private router: Router,
    private dialog: MatDialog
  ) {}

  public isSignedIn = false;

  public ngOnInit(): void {
    if (localStorage.getItem('user') != null) {
      this.isSignedIn = true;
      this.authService.isLoggedIn = true;
      this.router.navigate(['main']);
    }
  }

  public async signIn(email: string, password: string): Promise<void> {
    try {
      await this.authService.signIn(email, password);
      if (this.authService.isLoggedIn) {
        this.isSignedIn = true;
        this.router.navigate(['main']);
      }
    } catch {
      const dialogRef = this.dialog.open(incorrectDataPopupComponent);
    }
  }

  public async signUp(email: string, password: string): Promise<void> {
    await this.authService.signUp(email, password);
    if (this.authService.isLoggedIn) {
      this.isSignedIn = true;
      this.router.navigate(['main']);
    }
  }
}
