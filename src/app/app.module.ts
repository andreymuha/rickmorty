import { HeaderInterceptor } from './interceptors/app.interceptor';
import { AuthModule } from './auth/auth.module';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AngularFireModule } from '@angular/fire/compat';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { StoreModule } from '@ngrx/store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { EffectsModule } from '@ngrx/effects';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { DataEffects } from './store/effects/data.effects';
import { reducers } from './store';
import { ComponentsModule } from './components/components.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    AuthModule,
    AngularFireModule.initializeApp({
      apiKey: 'AIzaSyB1I0_8cmGb2WqKpfBdDCKnyqsAqH-RU8E',
      authDomain: 'rickmorty-ee9a6.firebaseapp.com',
      projectId: 'rickmorty-ee9a6',
      storageBucket: 'rickmorty-ee9a6.appspot.com',
      messagingSenderId: '490072905003',
      appId: '1:490072905003:web:19f3057a4a0245bb2da16e',
    }),
    StoreModule.forRoot(reducers),
    EffectsModule.forRoot([DataEffects]),
    StoreDevtoolsModule.instrument({}),
    HttpClientModule,
    ComponentsModule,
    BrowserAnimationsModule,
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HeaderInterceptor,
      multi: true,
    },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
