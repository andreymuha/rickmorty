import { LoginGuard } from './../guards/login.guard';
import { MainComponent } from './main.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', component: MainComponent, canActivate: [LoginGuard] },
  {
    path: 'change',
    loadChildren: () =>
      import('../changeDataInfo/changeData.module').then(
        (m) => m.ChangeDataModule
      ),
  },
  { path: '**', redirectTo: 'main' },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MainRoutingModule {}
