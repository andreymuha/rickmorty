import { HeaderInterceptor } from './../interceptors/app.interceptor';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { CommonModule } from '@angular/common';
import { MainRoutingModule } from './main-routing.module';
import { NgModule } from '@angular/core';
import { MainComponent } from './main.component';
import { ComponentsModule } from '../components/components.module';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { HTTP_INTERCEPTORS } from '@angular/common/http';

@NgModule({
  imports: [
    MainRoutingModule,
    CommonModule,
    NgxDatatableModule,
    ComponentsModule,
    MatProgressBarModule,
  ],
  exports: [],
  declarations: [MainComponent],
  providers: [],
})
export class MainModule {}
