import { routingAnimation } from './../animations/routing-animations';
import { SpinnerService } from './../services/spinner.service';
import { State } from './../store/index';
import {
  isUsersLoading,
  selectUsers,
} from './../store/selectors/data.selector';
import { AfterViewInit, Component, HostBinding, OnInit } from '@angular/core';
import { Observable, map, Subscription, async, tap } from 'rxjs';
import { select, Store } from '@ngrx/store';
import { getData } from '../store/actions/data.action';
import { ColumnMode, SelectionType } from '@swimlane/ngx-datatable';
import { Router } from '@angular/router';
import { LoadingBarService } from '@ngx-loading-bar/core';
import {
  trigger,
  state,
  style,
  animate,
  transition,
} from '@angular/animations';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss'],
  animations: [
    trigger('heroState', [
      state(
        'active',
        style({
          opacity: 100,
        })
      ),
      state(
        'not-active',
        style({
          opacity: 0,
        })
      ),
      transition('active => not-active', animate('3s ease-in')),
      transition('not-active => active', animate('3s ease-out')),
    ]),
    routingAnimation,
  ],
})
export class MainComponent implements OnInit, AfterViewInit {
  @HostBinding('@routingAnimation') private routing: any;
  constructor(
    public store: Store<State>,
    private router: Router,
    private loadingService: SpinnerService
  ) {
    this.loadingService.loading = true;
  }
  public readonly headerHeight = 50;
  public readonly rowHeight = 50;
  public readonly tableHeight = 750;
  public isSmthInStore: boolean = false;
  public showPopup: boolean = false;
  public bodyHeight = window.outerHeight;
  public averageHeight = 800;
  public ColumnMode = ColumnMode;
  public selectionType = SelectionType;
  public isLoading: Observable<boolean> = this.store.pipe(
    select(isUsersLoading)
  );

  public rows: Observable<Object> | undefined;
  public columns = [
    { name: 'Имя', prop: 'name' },
    { name: 'Пол', prop: 'gender' },
    { name: 'Статус', prop: 'status' },
  ];

  public ngOnInit(): void {
    let store = this.store.pipe(
      select(selectUsers),
      map((item) => Object.values(item))
    );
    store.subscribe((it) =>
      it.length > 0 ? (this.isSmthInStore = true) : (this.isSmthInStore = false)
    );
    if (this.isSmthInStore == false) {
      this.loadData();
    }
    this.rows = this.store.pipe(
      select(selectUsers),
      map((item) => Object.values(item))
    );
  }
  public loadData(): void {
    if (localStorage.getItem('user') !== null) {
      this.store.dispatch(getData());
    } else {
      this.router.navigate(['/auth']);
    }
  }

  public ngAfterViewInit(): void {
    this.loadingService.loading = false;
  }

  public onScroll(offset: number): void {
    if (this.bodyHeight - offset < this.averageHeight) {
      this.loadData();
      this.bodyHeight += 700;
    }
  }

  public onClick(e: any): void {
    this.router.navigate(['main/change'], {
      queryParams: { itemId: e.id },
    });
  }

  public searchCharacter(searchText: string): void {
    if (searchText.length == 0) {
      this.router.navigate(['/']);
    }
    this.rows = this.store.pipe(
      select(selectUsers),
      map((users) => Object.values(users)),
      map((users) =>
        users.filter(({ name }) => {
          this.router.navigate(['/main'], {
            queryParams: { name: searchText },
          });
          return name.toLowerCase().includes(searchText.toLowerCase());
        })
      )
    );
  }
}
