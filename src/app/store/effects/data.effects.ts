import { selectURL } from './../selectors/data.selector';
import { CharacterAPI } from './../../interfaces/character-api.interface';
import { DataService } from './../../services/data.service';
import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType, concatLatestFrom } from '@ngrx/effects';
import {
  catchError,
  combineLatest,
  concat,
  filter,
  forkJoin,
  map,
  mergeMap,
  Observable,
  of,
  tap,
} from 'rxjs';
import { Action, Store } from '@ngrx/store';
import {
  DataActions,
  getDataFailed,
  getDataSuccess,
  setURL,
} from '../actions/data.action';
import { HttpErrorResponse } from '@angular/common/http';
import { State } from '..';

@Injectable()
export class DataEffects {
  constructor(
    private actions: Actions,
    private dataService: DataService,
    private store: Store<State>
  ) {}

  private getData: Observable<Action> = createEffect(() =>
    this.actions.pipe(
      ofType(DataActions.GetData),
      concatLatestFrom(() => [this.store.select(selectURL)]),
      mergeMap(([action, url]) =>
        this.dataService.getData(url).pipe(
          mergeMap((characterApi) => [
            setURL({
              url: characterApi.info.next,
            }),
            getDataSuccess({ characters: characterApi.results }),
          ]),
          catchError((err: HttpErrorResponse) =>
            of(getDataFailed({ error: err.message }))
          )
        )
      )
    )
  );
}
