import { ActionReducerMap } from '@ngrx/store';
import * as dataReducer from './reducers/data.reducers';

export interface State {
  characters: dataReducer.CharacterState;
}

export const reducers: ActionReducerMap<State> = {
  characters: dataReducer.reducer,
};
