import { Character } from './../../interfaces/character.interface';
import { createAction, props } from '@ngrx/store';

export enum DataActions {
  GetData = '[DATA] Get',
  GetDataSuccess = '[DATA] Get success',
  GetDataFailed = '[DATA] Get failed',
  SetURL = '[URL] Set',
  TakeCharacter = '[DATA] Take character',
  SaveCharacter = '[DATA] Save character',
  DeleteCharacter = '[DATA] Delete character',
}

export const getData = createAction(DataActions.GetData);
export const getDataSuccess = createAction(
  DataActions.GetDataSuccess,
  props<{ characters: Character[] }>()
);
export const getDataFailed = createAction(
  DataActions.GetDataFailed,
  props<{ error: string }>()
);
export const setURL = createAction(
  DataActions.SetURL,
  props<{ url: string | null }>()
);

export const saveCharacter = createAction(
  DataActions.SaveCharacter,
  props<{ character: Character }>()
);

export const deleteCharacter = createAction(
  DataActions.DeleteCharacter,
  props<{ id: number }>()
);
