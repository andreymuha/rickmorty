import { environment } from './../../../environments/environment';
import {
  getData,
  getDataSuccess,
  getDataFailed,
  setURL,
  saveCharacter,
  deleteCharacter,
} from './../actions/data.action';
import { createReducer, on } from '@ngrx/store';
import { Character } from './../../interfaces/character.interface';
export interface CharacterState {
  characterArray: {
    [key: string]: Character;
  };
  url: string | null;
  error: string | null;
  isLoading: boolean;
}

const initialState: CharacterState = {
  characterArray: {},
  url: environment.baseURL,
  error: null,
  isLoading: false,
};

export const reducer = createReducer(
  initialState,
  on(getData, (state) => ({
    ...state,
    isLoading: true,
  })),
  on(getDataSuccess, (state, { characters }) => ({
    ...state,
    characterArray: characters.reduce(
      (acc: any, item: any) => {
        acc[item.id] = item;
        return acc;
      },
      { ...state.characterArray }
    ),
    error: null,
    isLoading: false,
  })),
  on(getDataFailed, (state, { error }) => ({
    ...state,
    error,
    characterArray: { ...state.characterArray },
    isLoading: false,
  })),
  on(setURL, (state, { url }) => ({
    ...state,
    url,
  })),
  on(saveCharacter, (state, { character }) => ({
    ...state,
    characterArray: { ...state.characterArray, [character.id]: character },
  })),
  on(deleteCharacter, (state, { id }) => ({
    ...state,
    characterArray: Object.values(state.characterArray).reduce(
      (acc: any, item: any) => {
        if (item.id != id) {
          acc[item.id] = item;
        }
        return acc;
      },
      {}
    ),
  }))
  // on(deleteCharacter, (state, { id }) => {
  //   const { [id]: value, ...characterArray } = state.characterArray;
  //   return {
  //     ...state,
  //     characterArray,
  //   };
  // })
);
