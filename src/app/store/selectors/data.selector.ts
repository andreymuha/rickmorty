import { CharacterState } from './../reducers/data.reducers';
import { State } from './../index';
import { createSelector } from '@ngrx/store';

export const selectDataFeature = (state: State) => state.characters;

export const isUsersLoading = createSelector(
  selectDataFeature,
  (state: CharacterState) => state.isLoading
);

export const selectUsers = createSelector(
  selectDataFeature,
  (state: CharacterState) => state.characterArray
);

export const selectError = createSelector(
  selectDataFeature,
  (state: CharacterState) => state.error
);

export const selectURL = createSelector(
  selectDataFeature,
  (state: CharacterState) => state.url
);

export const SelectUser = (id: number) =>
  createSelector(
    selectDataFeature,
    (state: CharacterState) => state.characterArray[id]
  );
