import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatDialogModule } from '@angular/material/dialog';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ComponentsModule } from '../components/components.module';
import { ChangeDataRoutingModule } from './changeData-routing.module';
import { ChangeDataComponent } from './changeData.component';

@NgModule({
  imports: [
    ChangeDataRoutingModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MatDialogModule,
  ],
  exports: [],
  declarations: [ChangeDataComponent],
  providers: [],
})
export class ChangeDataModule {}
