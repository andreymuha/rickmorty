import { routingAnimation } from './../animations/routing-animations';
import { PopupComponent } from './../components/popup/popup.component';
import { Character } from './../interfaces/character.interface';
import {
  ChangeDetectionStrategy,
  Component,
  HostBinding,
  OnInit,
} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Store, select, Action } from '@ngrx/store';
import { Observable, Subscription } from 'rxjs';
import { State } from '../store';
import { SelectUser } from '../store/selectors/data.selector';
import * as Actions from '../store/actions/data.action';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'app-data',
  templateUrl: 'changeData.component.html',
  styleUrls: ['./changeData.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: [routingAnimation],
})
export class ChangeDataComponent implements OnInit {
  @HostBinding('@routingAnimation') private routing: any;
  public item: number;
  public user: Character;
  public tempUser: Observable<Character>;
  private querySubscription: Subscription;
  public myForm: FormGroup;
  constructor(
    private route: ActivatedRoute,
    private store: Store<State>,
    private router: Router,
    private formBuilder: FormBuilder,
    private dialog: MatDialog
  ) {}

  public ngOnInit(): void {
    this.querySubscription = this.route.queryParams.subscribe(
      (queryParam: any) => {
        this.item = queryParam['itemId'];
      }
    );
    this.tempUser = this.store.pipe(select(SelectUser(this.item)));
    this.tempUser.subscribe((users: Character) => {
      this.user = users;
    });
    if (this.user === undefined) {
      this.router.navigate(['main']);
    } else {
      this.myForm = this.formBuilder.group({
        name: [this.user.name, [Validators.required]],
        gender: [this.user.gender, [Validators.required]],
        status: [this.user.status, [Validators.required]],
        species: [this.user.species, [Validators.required]],
        locName: [this.user.location.name, [Validators.required]],
        origName: [this.user.origin.name, [Validators.required]],
      });
    }
  }

  public ngOnDestroy(): void {
    this.querySubscription.unsubscribe();
  }

  public returnToMain(): void {
    this.router.navigate(['main']);
  }

  public saveUser(): void {
    const data: Character = {
      id: this.user.id,
      name: this.myForm.value.name,
      status: this.myForm.value.status,
      species: this.myForm.value.species,
      type: this.user.type,
      gender: this.myForm.value.gender,
      origin: {
        name: this.myForm.value.origName,
        url: this.user.origin.url,
      },
      location: {
        name: this.myForm.value.locName,
        url: this.user.location.url,
      },
      image: this.user.image,
      episode: this.user.episode,
      url: this.user.url,
      created: this.user.created,
    };
    this.store.dispatch(Actions.saveCharacter({ character: data }));
    this.router.navigate(['main']);
  }

  public openPopup(): void {
    const dialogRef = this.dialog.open(PopupComponent);

    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.router.navigate(['main']);
        this.store.dispatch(Actions.deleteCharacter({ id: this.user.id }));
      }
    });
  }
}
