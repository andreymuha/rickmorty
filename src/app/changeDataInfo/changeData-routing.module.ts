import { ChangeDataComponent } from './changeData.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', component: ChangeDataComponent },
  { path: '**', redirectTo: 'change' },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ChangeDataRoutingModule {}
