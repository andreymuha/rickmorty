import { routingAnimation } from './animations/routing-animations';
import { SpinnerService } from './services/spinner.service';
import {
  ChangeDetectionStrategy,
  Component,
  OnInit,
  AfterViewInit,
  HostBinding,
} from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: [routingAnimation],
})
export class AppComponent implements OnInit, AfterViewInit {
  @HostBinding('@routingAnimation') private routing: any;
  constructor(public loadingService: SpinnerService) {}
  public title = 'RickMorty';
  public ngOnInit(): void {
    this.loadingService.loading = true;
  }
  public ngAfterViewInit(): void {
    this.loadingService.loading = false;
  }
}
