import { CharacterEpisode } from './../interfaces/character-episode.interface';
import { Character } from './../interfaces/character.interface';
import { CharacterAPI } from './../interfaces/character-api.interface';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, throwError, tap, mergeMap } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class DataService {
  constructor(private httpClient: HttpClient) {}

  public getData(url: string | null): Observable<CharacterAPI | never> {
    return url
      ? this.httpClient.get<CharacterAPI>(url, { observe: 'body' })
      : throwError(() => new Error('Not a string!'));
  }
}
